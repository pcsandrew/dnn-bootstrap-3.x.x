﻿<%@ Control Language="vb" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/User.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="PRIVACY" Src="~/Admin/Skins/Privacy.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TERMS" Src="~/Admin/Skins/Terms.ascx" %>
<%@ Register TagPrefix="dnn" TagName="COPYRIGHT" Src="~/Admin/Skins/Copyright.ascx" %>
<%@ Register TagPrefix="dnn" TagName="JQUERY" Src="~/Admin/Skins/jQuery.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="BREADCRUMB" Src="~/Admin/Skins/breadcrumb.ascx" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>

<%--Register DDR Menu--%>
<%@ Register TagPrefix="ddr" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="ddr" Namespace="DotNetNuke.Web.DDRMenu.TemplateEngine" Assembly="DotNetNuke.Web.DDRMenu" %>


<!--jQuery -->
<dnn:JQUERY ID="dnnjQuery" runat="server" />

<dnn:META ID="mobileScale" runat="server" Name="viewport" Content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1" />

<!-- Fonts -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800' rel='stylesheet' type='text/css'>

<!-- Font Awesome -->
<dnn:DnnCssInclude ID="FontAwesome" runat="server" FilePath="fonts/FontAwesome/css/font-awesome.min.css" PathNameAlias="SkinPath" Priority="101" />


<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


<!-- Bootstrap's JavaScript plugins) -->
<dnn:DnnJsInclude ID="bootstrapJavascript" runat="server" FilePath="js/bootstrap.min.js" PathNameAlias="SkinPath" Priority="100" />

<!-- bxSlider Javascript file -->
<dnn:DnnJsInclude ID="bsSliderJS" runat="server" FilePath="libs/bxSlider/jquery.bxslider.min.js" PathNameAlias="SkinPath" Priority="102" />
<!-- bxSlider CSS file -->
<dnn:DnnCssInclude ID="bsSliderCSS" runat="server" FilePath="libs/bxSlider/jquery.bxslider.css" PathNameAlias="SkinPath" Priority="103" />

<!-- jQuery iosSlider -->
<dnn:DnnJsInclude ID="iosSlider" runat="server" FilePath="libs/iosSlider/jquery.iosslider.js" PathNameAlias="SkinPath" Priority="104" />

<!--Fancybox -->
<!-- Add mousewheel plugin (this is optional) -->
<dnn:DnnJsInclude ID="FancyboxMouseWheel" runat="server" FilePath="libs/fancybox/lib/jquery.mousewheel-3.0.6.pack.js" PathNameAlias="SkinPath" Priority="200" />
<!-- Add fancyBox -->
<dnn:DnnCssInclude ID="FancyboxCSS" runat="server" FilePath="libs/fancybox/source/jquery.fancybox.css?v=2.1.5" PathNameAlias="SkinPath" Priority="201" />
<dnn:DnnJsInclude ID="FancyboxJS" runat="server" FilePath="libs/fancybox/source/jquery.fancybox.pack.js?v=2.1.5" PathNameAlias="SkinPath" Priority="202" />
<!-- Optionally add helpers - button, thumbnail and/or media -->
<dnn:DnnCssInclude ID="FancyboxButton" runat="server" FilePath="libs/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" PathNameAlias="SkinPath" Priority="203" />
<dnn:DnnJsInclude ID="FancyboxButtonJS" runat="server" FilePath="libs/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5" PathNameAlias="SkinPath" Priority="204" />
<dnn:DnnJsInclude ID="FancyboxMediaJS" runat="server" FilePath="libs/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6" PathNameAlias="SkinPath" Priority="205" />
<dnn:DnnCssInclude ID="FancyboxThumbsCSS" runat="server" FilePath="libs/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" PathNameAlias="SkinPath" Priority="206" />
<dnn:DnnJsInclude ID="FancyboxThumbsJS" runat="server" FilePath="libs/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7" PathNameAlias="SkinPath" Priority="207" />


<!--This CSS File wants to be last, as it will correctly overwrite any changes you want to make to 3rd Party Plugins -->
<dnn:DnnCssInclude ID="general" runat="server" FilePath="main.min.css" PathNameAlias="SkinPath" Priority="999" />


<header>

    <div class="container">

        <div class="row">

            <div class="col-md-4">
                <dnn:LOGO runat="server" ID="dnnLogo" />

            </div>

            <div class="col-md-8">
            </div>

        </div>


        <div class="row">
            <ddr:MENU MenuStyle="MenuBootstrapDefault" runat="server" />
        </div>



        <div class="row">
            <ddr:MENU MenuStyle="MenuBreadcrumb" runat="server" />
        </div>
    </div>
</header>
